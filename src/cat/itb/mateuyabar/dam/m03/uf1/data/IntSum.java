package cat.itb.mateuyabar.dam.m03.uf1.data;

import java.util.Scanner;

/**
 * The user inputs two number and the sum of both is printed
 */
public class IntSum {
    public static void main(String[] args) {
        // Ask for two numbers number
        Scanner scanner = new Scanner(System.in);
        int firstInputValue = scanner.nextInt();
        int secondInputValue = scanner.nextInt();

        // Add the two numbers
        int result = firstInputValue + secondInputValue;

        // Print the result
        System.out.println(result);
    }
}
