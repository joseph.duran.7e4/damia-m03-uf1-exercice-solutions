package cat.itb.mateuyabar.dam.m03.uf1.data;

import java.util.Scanner;

/**
 * L'usuari escriu dos valors i s'imprimeix true si el primer
 * és més gran que el segon i false en qualsevol altre cas.
 */
public class FirstBigger {
    public static void main(String[] args){
        int resultat = 5;
        String text = "Resultat: "+resultat;
        System.out.println(text);

        // request two integer
        Scanner scanner = new Scanner(System.in);
        int value1 = scanner.nextInt();
        int value2 = scanner.nextInt();

        // value1 > value2 ?
        boolean result = value1 > value2;

        // print result
        System.out.println(result);
    }

}
