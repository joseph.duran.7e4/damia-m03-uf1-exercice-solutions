package cat.itb.mateuyabar.dam.m03.uf1.data;

import java.util.Scanner;

/**
 * L'usuari escriu un enter i s'imprimeix true si existeix un bitllet d'euros amb la quantitat entrada, false en qualsevol altre cas.
 */
public class IsValidNote {
    public static void main(String[] args) {
        // read a integer
        Scanner scanner = new Scanner(System.in);
        int noteValue = scanner.nextInt();

        // value in 5, 10, 20, 50, 100, 200 or 500
        boolean is5Note = noteValue == 5;
        boolean is10Note = noteValue == 10;
        boolean is20Note = noteValue == 20;
        boolean is50Note = noteValue == 50;
        boolean is100Note = noteValue == 100;
        boolean is200Note = noteValue == 200;
        boolean is500Note = noteValue == 500;

        boolean isNote = is5Note || is10Note || is20Note || is50Note  || is100Note  || is200Note  || is500Note;

        // print result
        System.out.println(isNote);
    }
}
