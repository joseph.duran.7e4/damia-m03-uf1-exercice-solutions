package cat.itb.mateuyabar.dam.m03.uf1.data;

import java.util.Scanner;

/**
 * The user inputs a number and the double is printed
 */
public class IntDoubleMe {
    public static void main(String[] args) {
        // Ask for one number
        Scanner scanner = new Scanner(System.in);
        int userInputValue = scanner.nextInt();

        // multilply the number by two
        int result = userInputValue * 2;

        // print the result
        System.out.println(result);
    }
}
