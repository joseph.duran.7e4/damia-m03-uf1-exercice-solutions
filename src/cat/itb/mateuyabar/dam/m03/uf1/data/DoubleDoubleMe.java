package cat.itb.mateuyabar.dam.m03.uf1.data;

import java.util.Scanner;

public class DoubleDoubleMe {
    public static void main(String[] args) {
        // request a number to the user
        Scanner scanner = new Scanner(System.in);
        double number = scanner.nextDouble();

        // multiply by two
        double result = number * 2.0;

        // print result
        System.out.println(result);
    }
}
